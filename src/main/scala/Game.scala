import scala.annotation.tailrec

case class Player(name: String, displayName: String)


object Game {

  type MutableMap[K, V] = scala.collection.mutable.Map[K, V]

  val START = 0
  val GOAL = 63
  val GOOSE = Seq(5, 9, 14, 18, 23, 27)
  val BRIDGE = Map(6 -> 12)

  var players: MutableMap[Player, Int] = scala.collection.mutable.Map[Player, Int]()

  def printPlayers: Unit = {
    println(s"players: ${players.keys.map(p => p.displayName).mkString(", ")}")
  }

  def addPlayer(cmdAdd: AddCommand): Unit = {
    if(cmdAdd.playerName.isEmpty || cmdAdd.playerName.equals("") || cmdAdd.playerName.equals("\'\'"))
      println(Console.RED + "the name of the player must be specified" + Console.RESET)
    else if(players.exists(p => p._1.name.equalsIgnoreCase(cmdAdd.playerName)))
      println(Console.RED + s"${cmdAdd.playerName}: already existing player" + Console.RESET)
    else
      players = players.+(Player(cmdAdd.playerName.toLowerCase, cmdAdd.playerName) -> 0); printPlayers
  }

  def resetPosition = {
    players = players.map{ elem => elem._1 -> 0}
    println(Console.GREEN + "All players have been placed at the Start. Good luck!!" + Console.RESET)
    printPlayers
    true
  }

  def nextRound = {
    if(players.exists(elem => elem._2 == GOAL)){
      println(Console.GREEN + "You want to play another game?")
      println("Write yes or y to play or anything else to finish" + Console.RESET)
      scala.io.StdIn.readLine().toLowerCase match {
        case "yes" | "y" => resetPosition
        case _           => false
      }
    } else true
  }

  def help = {
    val helpString = """
      |Commands allowed:
      |[ the commands are not case sensitive ]
      |
      |1) add player Pippo => use this command to add player Pippo to the game
      |2) move Pippo x, y  => use this command to move player Pippo by x + y positions
      |   x and y are the values of the dies. Every value is between 1 and 6.
      |   If you want, you can omitt the dies values, the game rolling dies for you!
      |3) exit             => use this command to exit
      |
    """.stripMargin
    println(Console.GREEN + helpString + Console.RESET)
  }

  def movePlayer(mvnCmd: MoveCommand) = {
    def checkDieValue(value: Int): Boolean = value > 0 && value < 7
    def generateRandomValue: Int = scala.util.Random.nextInt(6) + 1
    def printPos(pos: Int): String = if(pos == 0) "Start" else pos.toString
    def getPlayerNameByPos(pos: Int): String = players.filter( elem => elem._2 == pos).head._1.displayName

    @tailrec
    def calculateNextPosition(player: String, oldPos: Int, d1: Int, d2: Int, msg: String, goose: Boolean): (Int, String) = {
      val sum: Int = oldPos + d1 + d2
      val oldPosString: String = printPos(oldPos)
      if(BRIDGE.contains(sum)) {
        val bridgeValue = BRIDGE(sum)
        val str = s"$msg $player moves from $oldPosString to The Bridge. $player jumps to $bridgeValue"
        (bridgeValue, str)
      }
      else if(GOOSE.contains(sum)) {
        val str: String = if(goose) s"$msg $player moves again and goes to $sum, The Goose."
        else s"$msg $player moves from $oldPosString to $sum, The Goose."
        calculateNextPosition(player, sum, d1, d2, str, goose = true)
      }
      else if(players.values.toList.contains(sum)){
        val otherPlayer: String = getPlayerNameByPos(sum)
        val str: String = s"$msg $player moves from $oldPosString to $sum. On $sum there is $otherPlayer, who returns to $oldPosString"
        (oldPos, str)
      }
      else if(sum == GOAL) { (GOAL, s"$msg $player moves from $oldPosString to $GOAL. $player Wins!!") }
      else if(sum > GOAL) {
        val newPos: Int = GOAL - (sum - GOAL)
        (newPos, s"$msg $player moves from $oldPosString to $GOAL. $player bounces! $player returns to $newPos")
      }
      else {
        (sum, s"$msg $player moves from $oldPosString to $sum")
      }
    }

    val die1: Int = mvnCmd.die1.getOrElse(generateRandomValue)
    val die2: Int = mvnCmd.die2.getOrElse(generateRandomValue)

    if(!players.exists(elem => elem._1.displayName.equals(mvnCmd.player)))
      println(Console.RED + s"player ${mvnCmd.player} not found..." + Console.RESET)
    else if(checkDieValue(die1) && checkDieValue(die2)) {
      val player = Player(mvnCmd.player.toLowerCase, mvnCmd.player)
      val oldPos: Int = players(player)
      val str = s"${mvnCmd.player} rolls $die1, $die2."
      val newPositionNString: (Int, String) = calculateNextPosition(player.displayName, oldPos, die1, die2, str, goose = false)
      println(newPositionNString._2)
      players = players.updated(player, newPositionNString._1)
    } else {
      println(Console.RED + "the value of the die must be between 1 and 6" + Console.RESET)
    }
  }
}
