trait Command

sealed case class AddCommand(playerName: String) extends Command
sealed case class MoveCommand(player: String, die1: Option[Int], die2: Option[Int]) extends Command
sealed case class HelpCommand() extends Command
sealed case class ExitCommand() extends Command
sealed case class UnknownCommand() extends Command
