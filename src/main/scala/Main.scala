import scala.annotation.tailrec
import scala.util.Try

object Main extends App {

  @tailrec
  def loop: Unit  = {
    val line = scala.io.StdIn.readLine()
    parsingCommand(line.split(" ").head, line) match {
      case Left(error)           => println(Console.RED + error + Console.RESET); if(Game.nextRound) loop
      case Right(_: ExitCommand) => println(Console.GREEN + "Bye Bye!!" + Console.RESET)
      case Right(cmd)            =>
        executeCommand(cmd)
        if(Game.nextRound) loop else println(Console.GREEN + "Good game! Bey bey!" + Console.RESET)
    }
  }

  def executeCommand(cmd: Command) = {
    cmd match {
      case add: AddCommand   => Game.addPlayer(add)
      case mvn: MoveCommand  => Game.movePlayer(mvn)
      case _: HelpCommand    => Game.help
    }

  }

  def parsingCommand(commandName: String, str: String): Either[String, Command] = {
    commandName.toLowerCase match {
      case "add" =>
        val tryAddCommand = Try{
          val splitStr = str.split(" ")
          if(splitStr.length != 3 || !Try{splitStr(1)}.getOrElse("").equals("player"))
            Left("add command must be add player name_player")
          else Right(AddCommand(str.split(" ").last))
        }
        if(tryAddCommand.isSuccess) tryAddCommand.get
        else Left("add command must be add player name_player")
      case "move" =>
        val tryMoveCommand = Try{
          val splitStr: Array[String] = str.split(" ")
          if(splitStr.length > 2) MoveCommand(splitStr(1), Some(splitStr(2).replace(",", "").toInt), Some(splitStr(3).toInt))
          else MoveCommand(splitStr(1), None, None)
        }
        if(tryMoveCommand.isSuccess) Right(tryMoveCommand.get)
        else Left("move command must be move player_name or move player_name number, number")
      case "exit" =>
        Right(ExitCommand())
      case "help" =>
        Right(HelpCommand())
      case _ =>
        Left("command not found, use help to show guide")
    }
  }

  println(Console.GREEN + "Welcome to Goose Game!!")
  println("use 'help' command to show guide" + Console.RESET)
  loop
}