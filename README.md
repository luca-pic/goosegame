# GooseGame

[**Scala**](https://www.scala-lang.org) _implementation_ of the **Goose Game Kata**.

## Description

Goose game is a game where two or more players move pieces around a track by rolling a die. The aim of the game is to reach square number sixty-three before any of the other players and avoid obstacles ([wikipedia](https://en.wikipedia.org/wiki/Game_of_the_Goose)).

[Read more](GooseGame.md)

## Requirements

1. **Scala** (follows the instructions at [https://www.scala-lang.org/download](https://www.scala-lang.org/download)).
2. **sbt** (follows the instructions at  [https://www.scala-sbt.org/download.html](https://www.scala-sbt.org/download.html)).

## Installation

_Clone_ or _Download_ the **repository** and _enter_ to created **directory** (default name: **goosegame**)

### Clone

#### Linux

```console
git clone https://gitlab.com/luca-pic/goosegame
cd goosegame
```

#### Windows

```console
c:\..\..> git https://gitlab.com/luca-pic/goosegame
c:\..\..> cd goosegame
```

### Download

Use the [web page](https://gitlab.com/luca-pic/goosegame) to download project.

## Compile

### Linux

```console
~\goosegame$ sbt compile
```

### Windows

```console
c:\..\..\goosegame> sbt compile
```

## Execute

### Linux

1. Make sure the file _run.sh_ is **executable**.

2. If **not**, _run_

```console
~\goosegame$ chmod +x ./run.sh
```

## Execute

### Linux

```console
~\goosegame$ ./run.sh
```

### Windows

```console
c:\..\..\goosegame> run
```